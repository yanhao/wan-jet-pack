package com.yanhao;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class Demo1 {

    @Test
    public void map1() {
        Map<Integer, String> map = new HashMap<>();
        map.put(3, "333");
        map.put(4, "444");

        System.out.println(map.get(1));
        for (Map.Entry<Integer, String> d : map.entrySet()) {
            System.out.println("key= " + d.getKey() + " value= " + d.getValue());
        }
    }

}
