package com.yanhao.http

import com.yanhao.bean.*
import retrofit2.http.*

interface ApiService {

    /**
     * banner
     */
    @GET("banner/json")
    suspend fun getBanner(): WanResult<MutableList<BannerData>>

    /**
     * 首页文章
     */
    @GET("article/list/{pageIndex}/json")
    suspend fun getHomeArticles(@Path("pageIndex") pageIndex: Int): WanResult<Articles>

    /**
     * 置顶文章
     */
    @GET(value = "article/top/json")
    suspend fun getTopHomeArticles(): WanResult<MutableList<Article>>

    /**
     * 公众号
     */
    @GET(value = "wxarticle/chapters/json")
    suspend fun getWxArticles(): WanResult<MutableList<WxArticle>>

    /**
     * 某个公众号下属文章数据
     */
    @GET(value = "wxarticle/list/{id}/{indexPager}/json")
    suspend fun getWxArticleChapters(
        @Path("id") id: Int,
        @Path("indexPager") indexPager: Int
    ): WanResult<Chapterss>

    /**
     * 获取项目分类
     */
    @GET(value = "project/tree/json")
    suspend fun getProjectTree(): WanResult<MutableList<Project>>

    /**
     * 获取某个类型下的项目
     * /project/list/1/json?cid=294
     */
    @GET(value = "project/list/{index}/json")
    suspend fun getProjectTreeInfo(
        @Path("index") index: Int,
        @Query("cid") cid: Int
    ): WanResult<ProjectInfos>

    /**
     * 登陆
     */
    @FormUrlEncoded
    @POST(value = "user/login")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): WanResult<UserInfo>

//    /**
//     * 使用密封类
//     * 登陆
//     */
//    @FormUrlEncoded
//    @POST(value = "user/login")
//    suspend fun login(
//        @Field("username") username: String,
//        @Field("password") password: String
//    ): ApiResult<UserInfo>


    /**
     * 退出登陆
     */
    @GET(value = "user/logout/json")
    suspend fun logout(): WanResult<Any>

    /**
     * 获取积分
     */
    @GET(value = "lg/coin/userinfo/json")
    suspend fun getCoin(): WanResult<Coin>


    /**
     * 搜索
     */
    @POST(value = "article/query/{pageNum}/json")
    suspend fun search(@Path("pageNum") pageNum: Int, @Query("k") k: String): WanResult<Any>
}