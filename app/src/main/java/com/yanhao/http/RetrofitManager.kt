package com.yanhao.http

class RetrofitManager {

    companion object {
        /**
         * 用户保存apiserver
         */
        private val map = mutableMapOf<Class<*>, Any>()

        /**
         * 初始化retrofit
         */
        private val retrofit = RetrofitFactory.factory()

        /**
         * 动态指定service
         */
        fun <T : Any> getApiService(apiClass: Class<T>): T {
            return getService(apiClass)
        }


        private fun <T : Any> getService(apiClass: Class<T>): T {
            return if (map[apiClass] == null) {
                synchronized(RetrofitManager::class.java) {
                    val t = retrofit.create(apiClass)
                    if (map[apiClass] == null) {
                        map[apiClass] = t
                    }
                    t
                }
            } else {
                map[apiClass] as T
            }
        }


    }

}