package com.yanhao.utils

import android.content.Context

object Screent {
    var scale = 0f
    fun dpToPixel(dp: Float, context: Context): Int {
        if (scale == 0f) {
            scale = context.resources.displayMetrics.density
        }
        return (dp * scale).toInt()
    }
}