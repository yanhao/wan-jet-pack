package com.yanhao.utils

import com.google.gson.Gson
import com.tencent.mmkv.MMKV
import com.yanhao.bean.UserInfo


object MMKVUtils {

    val COOKIE = "cookie"
    val USERINFO = "UserInfo"

    val mmkv = MMKV.defaultMMKV()

    fun saveCook(value: Set<String>?) {
        mmkv?.encode(COOKIE, value)
    }

    fun getCook(): MutableSet<String>? = mmkv?.decodeStringSet(COOKIE)


    fun clearCook() {
        mmkv?.encode(COOKIE, "")
    }

    fun saveUserInfo(userInfo: UserInfo) {
        var userInfoJson = Gson().toJson(userInfo)
        mmkv.encode(USERINFO, userInfoJson)
//        mmkv?.encode("coinCount", userInfo.coinCount)
//        mmkv?.encode("email", userInfo.email)
//        mmkv?.encode("icon", userInfo.icon)
//        mmkv?.encode("id", userInfo.id)
//        mmkv?.encode("nickname", userInfo.nickname)
//        mmkv?.encode("publicName", userInfo.publicName)
//        mmkv?.encode("token", userInfo.token)
//        mmkv?.encode("type", userInfo.type)
//        mmkv?.encode("username", userInfo.username)
    }

    fun getUserInfo(): UserInfo {
        var decodeString = mmkv.decodeString(USERINFO)
        if (!decodeString.isNullOrBlank()) {
            return Gson().fromJson(decodeString, UserInfo::class.java)
        }
        return UserInfo()
    }

    fun clearUserInfo() {
        mmkv.encode(USERINFO, "")
    }

}