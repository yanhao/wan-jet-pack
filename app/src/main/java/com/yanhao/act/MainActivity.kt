package com.yanhao.act

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.gyf.immersionbar.ImmersionBar
import com.yanhao.R
import com.yanhao.act.fragment.HomeControlFragment
import com.yanhao.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    //https://juejin.cn/post/6844904079697657863#heading-18 fragment使用

    companion object {
        val FG_TAG = "MainActivity"
    }

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        ImmersionBar.with(this).transparentStatusBar().statusBarColor(R.color.color_30475E)
            .statusBarDarkFont(true)
            .fitsSystemWindows(true)
        supportFragmentManager.commit {
            add<HomeControlFragment>(containerViewId = R.id.fcv, tag = FG_TAG)
        }
    }

    override fun onResume() {
        super.onResume()
    }

}