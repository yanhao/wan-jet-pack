package com.yanhao.act

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.gyf.immersionbar.ImmersionBar
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.yanhao.R
import com.yanhao.base.BaseActivity
import com.yanhao.databinding.ActivitySplashBinding
import com.yanhao.etx.StartActPage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashActivity : BaseActivity() {
    companion object {
        val TAG = SplashActivity::class.java
    }

    var job: Job? = null

    private val binding by lazy {
        ActivitySplashBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        ImmersionBar.with(this).transparentStatusBar().statusBarColor(R.color.color_415d73)
            .statusBarDarkFont(true)
            .fitsSystemWindows(true)
        XXPermissions.with(this)
            .permission(Permission.READ_EXTERNAL_STORAGE)
            .permission(Permission.WRITE_EXTERNAL_STORAGE)
            .permission(Permission.CAMERA)
            .permission(Permission.READ_CALENDAR)
            .request(object : OnPermissionCallback {
                override fun onGranted(permissions: MutableList<String>?, all: Boolean) {
                    if (all) {
                        init()
                    } else {
                        Toast.makeText(
                            this@SplashActivity,
                            "获取部分权限成功，但部分授权未正常授予",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onDenied(permissions: MutableList<String>?, never: Boolean) {
                    super.onDenied(permissions, never)
                    if (never) {
                        XXPermissions.startPermissionActivity(this@SplashActivity, permissions)
                    }
                }

            })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == XXPermissions.REQUEST_CODE) {

        }
    }

    private fun init() {
        binding.tvTime.setOnClickListener {
            this.StartActPage<MainActivity>(this)
        }

        job = lifecycleScope.launch {
            //使用flow流开启倒计时
            flow {
                for (i in 5 downTo 0) {
                    emit(i)
                    delay(1000)
                }
            }.flowOn(Dispatchers.IO).onStart {

            }.catch {

            }.onCompletion {
                //取消flow协程
                cancelJob()
            }.flowOn(Dispatchers.Main).collect {
                binding.tvTime.text = "跳过${it}"
                if (it == 0) {
                    this@SplashActivity.StartActPage<MainActivity>(this@SplashActivity)
                }
            }
        }


//        job = lifecycleScope.launchWhenResumed {
//            //使用flow流开启倒计时
//            flow {
//                for (i in 5 downTo 0) {
//                    emit(i)
//                    delay(1000)
//                }
//            }.flowOn(Dispatchers.IO).onStart {
//
//            }.catch {
//
//            }.onCompletion {
//                //取消flow协程
//                cancelJob()
//            }.flowOn(Dispatchers.Main).collect {
//                binding.tvTime.text = "跳过${it}"
//                if (it == 0) {
//                    this@SplashActivity.StartActPage<MainActivity>(this@SplashActivity)
//                }
//            }
//        }
    }


    override fun setListener() {

    }

    override fun initLiveData() {

    }

    override fun initViewModel() {
    }


    override fun onStop() {
        super.onStop()
        //让下一个页面显示后再关闭当前页面
        finish()
    }

    //取消flow
    fun cancelJob() {
        if (job != null) {
            job?.cancel()
            job = null
        }
    }
}