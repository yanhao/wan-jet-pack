package com.yanhao.act.login

import android.os.Bundle
import android.os.UserManager
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.yanhao.bean.ApiResult
import com.yanhao.databinding.ActivityLoginBinding
import com.yanhao.view.LoadingDialog
import com.yanhao.viewmode.*

class ActivityLogin : AppCompatActivity() {
    companion object {

    }

    private val rootView by lazy {
        ActivityLoginBinding.inflate(layoutInflater)
    }

    private var loadDialog: LoadingDialog? = null

    private val vm by viewModels<LoginViewModel> {
        LoginViewModelFactory()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(rootView.root)
        initEvent()
        initObserve()
    }

    private fun initEvent() {
        rootView.btnLogin.setOnClickListener {
            val account = rootView.etAccount.text.toString().trim()
            val pwd = rootView.etPws.text.toString().trim()
            when {
                TextUtils.isEmpty(account) -> Toast.makeText(
                    this,
                    "请输入您的账户号",
                    Toast.LENGTH_SHORT
                ).show()
                TextUtils.isEmpty(pwd) -> Toast.makeText(
                    this,
                    "请输入您的密码",
                    Toast.LENGTH_SHORT
                ).show()
                else -> vm.login(account, pwd)
            }

        }
        rootView.rlTitlebar.setOnClickListener {
            finish()
        }
    }

    private fun initObserve() {
        vm.errorMsg.observe(this) {
            if (!it.isNullOrBlank()) {
                Toast.makeText(
                    this,
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        vm.loadding.observe(this) {
            if (it) {
                showLoading()
            } else {
                dismissLoading()
            }
        }

//        vm.userinfo.observe(this) {
//            when (it) {
//                is ApiResult.Success -> {
//                    Log.e("TAG", "成功了: ${it}")
//                }
//                is ApiResult.Faiure -> {
//                    Log.e("TAG", "失败了: ${it}")
//                }
//            }
//        }

        UserInfoLivedata.getInstance().observe(this) {
            if (it != null && it.id != 0) {
                finish()
            }
        }
    }

    fun showLoading() {
        if (loadDialog == null) {
            loadDialog = LoadingDialog(this)
            loadDialog?.setCancelable(false)
        }
        if (loadDialog?.isShowing == true) {
            return
        }
        loadDialog?.show()
    }

    fun dismissLoading() {
        if (loadDialog != null) {
            loadDialog?.dismiss()
            loadDialog = null
        }
    }


}