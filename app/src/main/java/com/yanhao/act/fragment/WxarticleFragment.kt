package com.yanhao.act.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.yanhao.act.WebInfoActivity
import com.yanhao.adapter.MyRecycledViewPool
import com.yanhao.adapter.WxarticleAdapter
import com.yanhao.databinding.FragmentWxarticleBinding
import com.yanhao.utils.WrapContentLinearLayoutManager
import com.yanhao.viewmode.WxarticleFfViewModel
import com.yanhao.viewmode.WxarticleFfViewModelFactory

private const val ARG_PARAM1 = "wxarticleId"

class WxarticleFragment : Fragment() {
    private var wxarticleId: Int = 0
    private var _binding: FragmentWxarticleBinding? = null
    private val binding get() = _binding!!
    private val myAdapter by lazy {
        WxarticleAdapter()
    }

    private val viewmodel by viewModels<WxarticleFfViewModel> {
        WxarticleFfViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWxarticleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            wxarticleId = it.getInt(ARG_PARAM1)
        }
        initView()
        initEvent()
        initData()
        initObserve()
    }

    companion object {

        @JvmStatic
        fun newInstance(wxarticleId: Int) =
            WxarticleFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, wxarticleId)
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    //
//    override fun lazyInit() {
//        arguments?.let {
//            wxarticleId = it.getInt(ARG_PARAM1)
//        }
//        initView()
//        initData()
//    }
//
    fun initView() {
        binding.refreshLayout.setEnableRefresh(true)//启用下拉刷新
        binding.refreshLayout.setEnableLoadMore(true)//启用加载更多
        binding.refreshLayout.setEnableOverScrollBounce(true)

        binding.recyclerview.apply {
//            this.layoutManager = LinearLayoutManager(requireContext()).apply {
//                recycleChildrenOnDetach = true
//            }

            this.layoutManager = WrapContentLinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            ).apply {
                recycleChildrenOnDetach = true
            }

            recycledViewPool.setMaxRecycledViews(0, 10)
            setRecycledViewPool(recycledViewPool)


            this.adapter = myAdapter

        }
    }

    fun initData() {
        viewmodel.getWxarticleDatas(wxarticleId)
    }

    fun initEvent() {
        myAdapter.setOnItemClickListener { i, view, chapters ->
            val intent = Intent(requireContext(), WebInfoActivity::class.java).apply {
                putExtra(WebInfoActivity.LINK, chapters.link)
                putExtra(WebInfoActivity.TITLE, chapters.title)
            }
            startActivity(intent)
        }
        myAdapter.setOnItemChildClickListener { i, view, chapters ->
            viewmodel.collectArticle(chapters.id)
        }
        binding.refreshLayout.setOnRefreshListener {
            viewmodel.refreshCurrent(wxarticleId)
        }

        binding.refreshLayout.setOnLoadMoreListener {
            viewmodel.onLoadMoreCurrent(wxarticleId)
        }
    }

    private fun initObserve() {
        viewmodel.chapters.observe(viewLifecycleOwner) { datas ->
            datas?.let {
                binding.refreshLayout.finishRefresh(0)
                binding.refreshLayout.finishLoadMore(0)
                myAdapter.submitList(it)
            }
        }
    }
}