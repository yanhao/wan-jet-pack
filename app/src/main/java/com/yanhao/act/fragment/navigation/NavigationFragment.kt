package com.yanhao.act.fragment.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.yanhao.act.fragment.WxarticleFragment
import com.yanhao.databinding.FragmentNavigationBinding
import com.yanhao.viewmode.NavigationFgViewModel
import com.yanhao.viewmode.NavigationFgViewModelFactory
import kotlinx.coroutines.launch

class NavigationFragment : Fragment() {
    private var _binding: FragmentNavigationBinding? = null;
    private val binding get() = _binding!!

    private val viewmodel by viewModels<NavigationFgViewModel> {
        NavigationFgViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNavigationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        lifecycleScope.launch {
            viewmodel.getWxArticlesData()
        }
    }


    private fun initView() {
        viewmodel.wxArticles.observe(viewLifecycleOwner) {
            it?.let {
                val fragments = mutableListOf<Fragment>()
                val titles = mutableListOf<String>()
                it.forEach { wxArticle ->
                    titles.add(wxArticle.name)
                    binding.tablayout.addTab(binding.tablayout.newTab().setText(wxArticle.name))
                    fragments.add(WxarticleFragment.newInstance(wxarticleId = wxArticle.id))
                }
//                binding.viewpager.offscreenPageLimit = 0
                binding.viewpager.adapter = object : FragmentStatePagerAdapter(
                    childFragmentManager,
                    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
                ) {
                    override fun getCount(): Int = fragments.size

                    override fun getItem(position: Int): Fragment = fragments[position]

                    override fun getPageTitle(position: Int): CharSequence = titles[position]

                }
                binding.tablayout.setupWithViewPager(binding.viewpager, false)

//                binding.viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
//                    override fun onPageScrolled(
//                        position: Int,
//                        positionOffset: Float,
//                        positionOffsetPixels: Int
//                    ) {
//
//                    }
//
//                    override fun onPageSelected(position: Int) {
//                        viewmodelWx.setWxarticleId(it[position].id)
//                    }
//
//                    override fun onPageScrollStateChanged(state: Int) {
//                    }
//
//                })

                //TODO --> SlidingTabLayout
//                binding.viewpager.adapter = object : FragmentPagerAdapter(
//                    childFragmentManager,
//                    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
//                ) {
//                    override fun getCount(): Int {
//                        return it.size
//                    }
//
//                    override fun getItem(position: Int): Fragment {
//                        return WxarticleFragment.newInstance(wxarticleId = it[position].id)
//                    }
//
//                    override fun getPageTitle(position: Int): CharSequence = titles[position]
//
//                }
//                binding.slidetablayout.setViewPager(
//                    binding.viewpager,
//                    titles.toTypedArray(),
//                    requireActivity(),
//                    fragments as ArrayList<Fragment>?
//                )

            }
        }


    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
    }
}