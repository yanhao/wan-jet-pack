package com.yanhao.act.fragment.me

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.yanhao.act.login.ActivityLogin
import com.yanhao.databinding.FragmentMeBinding
import com.yanhao.view.LoadingDialog
import com.yanhao.viewmode.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MeFragment : Fragment() {
    companion object {
        @JvmStatic
        fun newInstance() = MeFragment()
    }

    private var _binding: FragmentMeBinding? = null
    private val binding get() = _binding!!
    private var loadDialog: LoadingDialog? = null
    private val vm by viewModels<MeFgViewModel> {
        MeFgViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEvent()
        initObserve()
    }

    private fun initEvent() {
        binding.clHear.setOnClickListener {
//            val value = vm.userInfo.value
            var value = UserInfoLivedata.getInstance().value
            if (value == null || value.id == 0) {
                //登陆
                val intent = Intent(requireContext(), ActivityLogin::class.java)
                startActivity(intent)
            }
        }
        binding.btnLogout.setOnClickListener {
            vm.logout()
        }
    }

    private fun initObserve() {
        UserInfoLivedata.getInstance().observe(viewLifecycleOwner) {
            if (it != null && it.id != 0) {
                binding.tvName.text = it.nickname
                binding.tvId.text = "${it.id}"
                binding.btnLogout.visibility = View.VISIBLE
                vm.getCoin()
            } else {
                binding.tvName.text = "请登录"
                binding.tvId.text = ""
                binding.btnLogout.visibility = View.GONE
            }
        }

        lifecycleScope.launch {
//            vm._coin.collect {
//                binding.tvRanking.text = "${it.rank}"
//                binding.tvIntegral.text = "${it.coinCount}"
//            }
            vm.coin.collect {
                binding.tvRanking.text = "${it.rank}"
                binding.tvIntegral.text = "${it.coinCount}"
            }
        }

//        vm.coin.observe(viewLifecycleOwner) {
//            binding.tvRanking.text = "${it.rank}"
//            binding.tvIntegral.text = "${it.coinCount}"
//
//        }
        vm.errorMsg.observe(viewLifecycleOwner) {
            if (!it.isNullOrBlank()) {
                Toast.makeText(
                    requireContext(),
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        vm.loadingCoin.observe(viewLifecycleOwner) {
            if (it) {
                showLoading()
            } else {
                dismissLoading()
            }
        }
    }

    fun showLoading() {
        if (loadDialog == null) {
            loadDialog = LoadingDialog(requireContext())
            loadDialog?.setCancelable(false)
        }
        if (loadDialog?.isShowing == true) {
            return
        }
        loadDialog?.show()
    }

    fun dismissLoading() {
        if (loadDialog != null) {
            loadDialog?.dismiss()
            loadDialog = null
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}