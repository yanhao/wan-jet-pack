package com.yanhao.act.fragment.project

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.yanhao.R
import com.yanhao.act.WebInfoActivity
import com.yanhao.adapter.ProjectInfoAdapter
import com.yanhao.bean.ProjectInfo
import com.yanhao.databinding.FragmentProjectInfoBinding
import com.yanhao.viewmode.ProjectInfoModel
import com.yanhao.viewmode.ProjectInfoModelFactory

private const val ARG_PARAM1 = "title"
private const val ARG_PARAM2 = "cid"

class ProjectInfoFragment : Fragment() {
    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: Int) =
            ProjectInfoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putInt(ARG_PARAM2, param2)
                }
            }
    }

    // TODO: Rename and change types of parameters
    private var title: String? = null
    private var cid: Int = 0

    private var _rootView: FragmentProjectInfoBinding? = null
    private val rootView get() = _rootView!!

    private val vm by viewModels<ProjectInfoModel> {
        ProjectInfoModelFactory()
    }
    private var datas = mutableListOf<ProjectInfo>()

    private val myAdapter by lazy {
        ProjectInfoAdapter(requireContext(), datas)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_PARAM1)
            cid = it.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _rootView = FragmentProjectInfoBinding.inflate(inflater, container, false)
        return rootView.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rootView.refreshLayout.setEnableRefresh(true)//启用下拉刷新
        rootView.refreshLayout.setEnableLoadMore(true)//启用加载更多
        rootView.refreshLayout.setEnableOverScrollBounce(true)
        initEvent()
        rootView.recyclerview?.apply {
            this.layoutManager = LinearLayoutManager(requireActivity())
            val divider = DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
            ContextCompat.getDrawable(requireContext(), R.drawable.divider)?.let {
                divider.setDrawable(
                    it
                )
            }
            addItemDecoration(
                divider
            )
            this.adapter = myAdapter
        }
        vm.getProjectTrees(cid = cid)
        initObserve()
    }

    private fun initObserve() {
        vm.projectInfos.observe(viewLifecycleOwner) {
            if (vm.isRefresh.value == true) {
                myAdapter.setNewData(it)
            } else {
                myAdapter.addData(it)
            }
            rootView.refreshLayout.finishRefresh(0)
            rootView.refreshLayout.finishLoadMore(0)
        }
    }

    private fun initEvent() {
        rootView.refreshLayout.setOnRefreshListener {
            vm.refreshData(cid)
        }

        rootView.refreshLayout.setOnLoadMoreListener {
            vm.onLoadMoreData(cid)
        }
        myAdapter.apply {
            setOnItemClickListener { _, _, projectInfo ->
                val intent = Intent(requireContext(), WebInfoActivity::class.java).apply {
                    putExtra(WebInfoActivity.LINK, projectInfo.link)
                    putExtra(WebInfoActivity.TITLE, projectInfo.title)
                }
                startActivity(intent)
            }
            setOnItemChildClickListener { i, view, projectInfo ->

            }
        }
    }


}