package com.yanhao.act.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.yanhao.R
import com.yanhao.act.fragment.home.HomeFragment
import com.yanhao.act.fragment.me.MeFragment
import com.yanhao.act.fragment.navigation.NavigationFragment
import com.yanhao.act.fragment.project.ProjectFragment
import com.yanhao.databinding.FragmentHomeControlBinding
import com.yanhao.viewmode.HomeControlViewModel

class HomeControlFragment : Fragment() {

    companion object {
    }

    private var _binding: FragmentHomeControlBinding? = null
    private val binding get() = _binding!!
    private var tvBottoms: MutableList<TextView> = mutableListOf()
    private var mFragments: MutableList<Fragment> = mutableListOf()
    private var currentFragment: Fragment? = null

    private val viewModel: HomeControlViewModel by viewModels()

    private val mHomeFragment by lazy {
        HomeFragment()
    }
    private val mNavigationFragment by lazy {
        NavigationFragment()
    }
    private val mProjectFragment by lazy {
        ProjectFragment.newInstance()
    }
    private val mMeFragment by lazy {
        MeFragment.newInstance()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeControlBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewPage()

    }

    private fun initView() {
        tvBottoms?.apply {
            add(binding.bottomView.tvHome)
            add(binding.bottomView.tvNavigation)
            add(binding.bottomView.tvProject)
            add(binding.bottomView.tvMe)
        }
        tvBottoms?.let {
            for (tv in tvBottoms) {
                tv.setOnClickListener {
                    when (it.id) {
                        R.id.tv_home -> fragmentManager(0)
                        R.id.tv_navigation -> fragmentManager(1)
                        R.id.tv_project -> fragmentManager(2)
                        R.id.tv_me -> fragmentManager(3)
                    }
                }
            }
        }
    }

    private fun initViewPage() {
        mFragments?.apply {
            add(getCurrentFragment(0)!!)
            add(getCurrentFragment(1)!!)
            add(getCurrentFragment(2)!!)
            add(getCurrentFragment(3)!!)
        }
        viewModel.currentIndex.value.let {
            fragmentManager(it ?: 0)
        }
    }

    private fun fragmentManager(index: Int) {
        viewModel.setCurrentIndex(index)
        val targetFg: Fragment = mFragments[index]
        val transaction = childFragmentManager.beginTransaction()
        transaction.apply {
            currentFragment?.let {
                hide(currentFragment!!)
            }
            setReorderingAllowed(true)
            if (!targetFg.isAdded) {
                add(R.id.fcv, targetFg).commit()
            } else {
                show(targetFg).commit()
            }
        }
        currentFragment = targetFg

        for (i in tvBottoms.indices) {
            tvBottoms[i].isSelected = index == i
        }
    }

    private fun getCurrentFragment(index: Int): Fragment? {
        return when (index) {
            0 -> mHomeFragment
            1 -> mNavigationFragment
            2 -> mProjectFragment
            3 -> mMeFragment
            else -> null
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}