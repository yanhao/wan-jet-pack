package com.yanhao.act.fragment.home

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import com.yanhao.databinding.ActivitySearchBinding
import com.yanhao.viewmode.SearchViewModel
import com.yanhao.viewmode.SearchViewModelFactory
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect as collect1


class ActivitySearch : AppCompatActivity() {

    private val rootView by lazy {
        ActivitySearchBinding.inflate(layoutInflater)
    }

    private val vm by viewModels<SearchViewModel> {
        SearchViewModelFactory()
    }
    private val _etState = MutableStateFlow("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(rootView.root)
        rootView.etcontent.addTextChangedListener {
            val result = it.toString()
            vm.queryParameterForDb(result)
        }

        vm.searchResult.observe(this) {
            if(it.errorCode==0){
                Log.e("TAG", "搜索数据是: ${it.data}", )
            }
        }

//        rootView.etcontent.addTextChangedListener { text ->
//            _etState.value = (text ?: "").toString()
//        }
//        lifecycleScope.launch {
//            _etState.sample(1000)
//                .filter {
//                    //空文本过滤掉
//                    it.isNotBlank()
//                }.collect {
//                    //订阅数据
//                    print("=======================>$it")
//                }
//        }
    }

}