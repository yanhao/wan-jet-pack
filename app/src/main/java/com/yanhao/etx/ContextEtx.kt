package com.yanhao.etx

import android.content.Context
import android.content.Intent
import android.os.Bundle

inline fun <reified T : Context> Context.StartActPage(ctx: Context) {
    val intent = Intent(ctx, T::class.java)
    startActivity(intent)
}

inline fun <reified T : Context> Context.StartActPage(ctx: Context, bundle: Bundle) {
    val intent = Intent(ctx, T::class.java)
    intent.putExtras(bundle)
    startActivity(intent)
}


