package com.yanhao.etx

import android.widget.TextView

inline fun TextView.text(content: String) {
    if (!content.isNullOrEmpty()) {
        this.text = content
    }
}
