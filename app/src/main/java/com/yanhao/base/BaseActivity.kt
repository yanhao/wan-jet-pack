package com.yanhao.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.yanhao.AppManager

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppManager.instance.addActivity(this)
        //设置livedata
        initLiveData()
        //设置监听
        setListener();
    }

    abstract fun setListener()

    abstract fun initLiveData()

    abstract fun initViewModel()

    override fun onDestroy() {
        super.onDestroy()
        //自己再合适的场景下关闭
//        AppManager.instance.removeActivity(this)
    }

}