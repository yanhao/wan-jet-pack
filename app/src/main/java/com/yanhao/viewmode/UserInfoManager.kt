package com.yanhao.viewmode

import com.yanhao.bean.UserInfo

class UserInfoManager {
    private val listeners = mutableListOf<OnUserInfoDataChangeListener>()

    fun setListener(listener: OnUserInfoDataChangeListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: OnUserInfoDataChangeListener) {
        listeners.remove(listener)
    }

    interface OnUserInfoDataChangeListener {
        fun change(userInfo: UserInfo)
    }
}