package com.yanhao.viewmode

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WebFragmentViewModel : BaseViewModel() {
    /**
     * 进度条进度
     */
    private var _maxprogress = MutableLiveData<Int>(0)
    val maxprogress: LiveData<Int> = _maxprogress

    /**
     * progress是否隐藏
     */
    private var _isVisible = MutableLiveData<Boolean>(false)
    val isVisible: LiveData<Boolean> = _isVisible

    fun setProgress(process: Int) {
        _maxprogress.value = process
    }

    fun setProgress(): Int = _maxprogress.value ?: 0

    fun progressShow() {
        _isVisible.value = false
    }

    fun progressHide() {
        _isVisible.value = true
    }

}