package com.yanhao.viewmode

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeControlViewModel : BaseViewModel() {

    //    private val _currentIndx: MutableLiveData<Int> = MutableLiveData()
    private val _currentIndx = MutableLiveData(0)
    val currentIndex: LiveData<Int> = _currentIndx

    fun setCurrentIndex(index: Int) {
        _currentIndx.postValue(index)
    }

    fun getCurrentIndex(): Int {
        return _currentIndx.value ?: 0
    }

}