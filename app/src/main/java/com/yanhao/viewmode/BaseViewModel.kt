package com.yanhao.viewmode

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yanhao.bean.ApiResult
import com.yanhao.bean.WanResult
import com.yanhao.etx.error

open class BaseViewModel : ViewModel() {

    /**
     * 错误信息
     */
    var _errorMsg = MutableLiveData<String>()
    val errorMsg: LiveData<String>
        get() = _errorMsg


    /**
     * 是否显示loadding
     */
    var _loadding = MutableLiveData<Boolean>()
    val loadding: LiveData<Boolean>
        get() = _loadding


    /**
     * 是否正在加载数据
     */
    var _loadDatas = MutableLiveData<Boolean>()
    val loadDatas: LiveData<Boolean>
        get() = _loadDatas


    /**
     * 是否是第一次加载
     * 默认是第一次加载
     */
    var _isFirstLoad = MutableLiveData(false)
    val isFirstLoad: LiveData<Boolean>
        get() = _isFirstLoad


    fun refreshFirstLoad(firstLoad: Boolean) {
        _isFirstLoad.value = firstLoad
    }

    /**
     * 处理异常信息
     */
    fun errorCatch(e: Throwable) {
        _loadding.value = false
        val errorData = e.error()
        errorData?.let {
            _errorMsg.value = errorData.msg
        }
    }


//    if (it.errorCode == 0) {
//        _bannerData.value =
//            ApiResult.Success(it.data)
//    } else {
//        _bannerData.value =
//            ApiResult.Faiure(errorCode = it.errorCode, errorMsg = it.errorMsg)
//    }


}