package com.yanhao.viewmode

import androidx.lifecycle.*
import com.yanhao.bean.Coin
import com.yanhao.bean.WanResult
import com.yanhao.etx.error
import com.yanhao.repository.UserInfoRepository
import com.yanhao.utils.MMKVUtils
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.launch
import java.lang.reflect.WildcardType

class MeFgViewModel(private val resp: UserInfoRepository) : BaseViewModel() {

//    private var _coin = MutableLiveData<Coin>()
//    val coin: LiveData<Coin>
//        get() = _coin

    //    var _coin = MutableStateFlow(Coin())
    private var _coin = MutableStateFlow(Coin())
    val coin: StateFlow<Coin>
        get() = _coin

    private var _loadingCoin = MutableLiveData<Boolean>()
    val loadingCoin: LiveData<Boolean>
        get() = _loadingCoin

    fun logout() {
        viewModelScope.launch {
            flow {
                val result = resp.logout()
                emit(result)
            }.catch { e: Throwable ->
                errorCatch(e)
            }.onStart {
                _loadding.value = true
            }.collect {
                _loadding.value = false
                if (it.errorCode == 0) {
                    MMKVUtils.clearCook()
                    MMKVUtils.clearUserInfo()
                    _coin.value = Coin(0, 0, 0, "")
//                    _userInfo.value = MMKVUtils.getUserInfo()
                    UserInfoLivedata.getInstance().postData(MMKVUtils.getUserInfo())
                } else {
                    _errorMsg.value = it.errorMsg
                }
            }
        }
    }

    fun getCoin() {
        viewModelScope.launch {
            flow {
                val result = resp.getCoin()
                emit(result)
            }.catch { e: Throwable ->
                errorCatch(e)
            }.onStart {
                _loadingCoin.value = true
            }.collect {
                _loadingCoin.value = false
                if (it.errorCode == 0) {
                    val data = it.data
                    _coin.value = data
                } else if (it.errorCode == -1001) {
                    MMKVUtils.clearCook()
                    MMKVUtils.clearUserInfo()
                    _coin.value = Coin(0, 0, 0, "")
//                    _userInfo.value = MMKVUtils.getUserInfo()
                    MMKVUtils.getUserInfo()?.let { userinfo ->
                        UserInfoLivedata.getInstance().postData(userinfo)
                    }
                } else {
                    _errorMsg.value = it.errorMsg
                }
            }
        }
    }

}

class MeFgViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MeFgViewModel::class.java)) {
            return MeFgViewModel(resp = UserInfoRepository()) as T
        }
        throw IllegalArgumentException("不能找到 MeFgViewModelFactory")
    }

}