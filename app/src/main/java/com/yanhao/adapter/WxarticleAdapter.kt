package com.yanhao.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yanhao.R
import com.yanhao.bean.Chapters
import com.yanhao.databinding.ItemWxarticleAdapterBinding

class MyRecycledViewPool : RecyclerView.RecycledViewPool() {

    var num = 0
    override fun getRecycledView(viewType: Int): RecyclerView.ViewHolder? {
        return super.getRecycledView(viewType)
    }

    override fun putRecycledView(scrap: RecyclerView.ViewHolder?) {
        super.putRecycledView(scrap)
    }
}


class WxarticleAdapter : ListAdapter<Chapters, WxarticleAdapter.MyHolder>(DiffChapters()) {

    private var mOnItemClickListener: ((Int, View, Chapters) -> Unit)? = null

    private var mOnItemChildClickListener: ((Int, View, Chapters) -> Unit)? = null

    fun setOnItemClickListener(onItemClickListener: ((Int, View, Chapters) -> Unit)? = null) {
        this.mOnItemClickListener = onItemClickListener
    }

    fun setOnItemChildClickListener(onItemChildClickListener: ((Int, View, Chapters) -> Unit)? = null) {
        this.mOnItemChildClickListener = onItemChildClickListener
    }

    class DiffChapters : DiffUtil.ItemCallback<Chapters>() {
        override fun areItemsTheSame(oldItem: Chapters, newItem: Chapters): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Chapters, newItem: Chapters): Boolean {
            return oldItem.id == newItem.id
                    && oldItem.niceShareDate == newItem.niceShareDate
                    && oldItem.collect == newItem.collect
        }

    }

    class MyHolder(private val rootView: ItemWxarticleAdapterBinding) :
        RecyclerView.ViewHolder(rootView.root) {
        fun getRoot(): ItemWxarticleAdapterBinding = rootView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view =
            ItemWxarticleAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val chapters = getItem(position)
        holder.getRoot().tvName.text = chapters.author
        holder.getRoot().tvTime.text = chapters.niceShareDate
        holder.getRoot().tvTitle.text = chapters.title
        holder.getRoot().tvForm.text = chapters.superChapterName
        if (chapters.collect) {
            holder.getRoot().ivCollect.setBackgroundResource(R.drawable.ic_collect_checked)
        } else {
            holder.getRoot().ivCollect.setBackgroundResource(R.drawable.ic_collect_unchecked_stroke)
        }
        holder.itemView.setOnClickListener {
            mOnItemClickListener?.invoke(position, it, chapters)
        }
        holder.getRoot().ivCollect.setOnClickListener {
            mOnItemChildClickListener?.invoke(
                position, it, chapters
            )
        }
    }
}