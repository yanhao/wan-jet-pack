package com.yanhao.adapter

import android.content.Context
import android.renderscript.ScriptGroup
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.yanhao.bean.BannerData
import com.yanhao.databinding.ItemMainBannerBinding
import com.yanhao.utils.MyAppGlideModule
import com.youth.banner.adapter.BannerAdapter

class MainBannerAdapter(val ctx: Context, val mData: MutableList<BannerData>) :
    BannerAdapter<BannerData, MainBannerAdapter.BannerViewHolder>(mData) {


    class BannerViewHolder(private val binding: ItemMainBannerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun getBind(): ItemMainBannerBinding = binding
    }

    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): BannerViewHolder {
        val view =
            ItemMainBannerBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
        return BannerViewHolder(view)
    }

    override fun onBindView(
        holder: BannerViewHolder?,
        data: BannerData?,
        position: Int,
        size: Int
    ) {
        holder?.getBind()?.let {
            Glide.with(ctx).load(mData[position].imagePath).into(it.ivBanner) }
    }

}