package com.yanhao.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.yanhao.R
import com.yanhao.bean.ProjectInfo
import com.yanhao.databinding.ItemLayoutProjectinfoBinding
import com.yanhao.etx.text
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class ProjectInfoAdapter(private val ctx: Context, private val datas: MutableList<ProjectInfo>) :
    RecyclerView.Adapter<ProjectInfoAdapter.MyViewHolder>() {

    private var mOnItemClickListener: ((Int, View, ProjectInfo) -> Unit)? = null
    private var mOnItemChildClickListener: ((Int, View, ProjectInfo) -> Unit)? = null

    fun setOnItemClickListener(onItemClickListener: ((Int, View, ProjectInfo) -> Unit)? = null) {
        this.mOnItemClickListener = onItemClickListener
    }

    fun setOnItemChildClickListener(onItemChildClickListener: ((Int, View, ProjectInfo) -> Unit)? = null) {
        this.mOnItemChildClickListener = onItemChildClickListener
    }


    class MyViewHolder(private val rootView: ItemLayoutProjectinfoBinding) :
        RecyclerView.ViewHolder(rootView.root) {
        fun getRoot(): ItemLayoutProjectinfoBinding = rootView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemLayoutProjectinfoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val root = holder.getRoot()
        val itemData = datas[position]
        itemData?.let {
            root.tvTitle.text(it.title)
            root.tvDesc.text(it.desc)
            Glide.with(ctx).load(it.envelopePic)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(20))).into(root.ivIcon)
            root.tvTime.text(it.niceDate)
            if (itemData.collect) {
                root.ivCollect.setBackgroundResource(R.drawable.ic_collect_checked)
            } else {
                root.ivCollect.setBackgroundResource(R.drawable.ic_collect_unchecked_stroke)
            }
        }
        holder.itemView.setOnClickListener {
            mOnItemClickListener?.invoke(position, it, itemData)
        }
        root.ivCollect.setOnClickListener {
            mOnItemChildClickListener?.invoke(position, it, itemData)
        }
    }

    override fun getItemCount(): Int = datas.size

    fun setNewData(newData: MutableList<ProjectInfo>? = null) {
        this.datas.clear()
        if (newData != null) {
            this.datas.addAll(newData)
        }
        notifyDataSetChanged()
    }

    fun addData(newData: MutableList<ProjectInfo>) {
        this.datas.addAll(newData)
        notifyItemRangeChanged(datas.size - newData.size, newData.size)
    }


//    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
//        unCollectJob?.cancel()
//        super.onDetachedFromRecyclerView(recyclerView)
//    }
//
//    private var unCollectJob: Job? = null

//    private fun unCollect(id: String): MutableLiveData<HttpResponse> {
//        val result = MutableLiveData<HttpResponse>()
//        unCollectJob = CoroutineScope(Dispatchers.Main).launch {
//            val request = HttpRequest("lg/uncollect_originId/{id}/json")
//            request.putPath("id", id)
//            result.postValue(post(request))
//        }
//        return result
//    }

}
