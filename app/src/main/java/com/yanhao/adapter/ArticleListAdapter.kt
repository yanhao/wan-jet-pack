package com.yanhao.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yanhao.R
import com.yanhao.bean.Article
import com.yanhao.databinding.ItemArticleAdapterBinding

class ArticleListAdapter : ListAdapter<Article, ArticleListAdapter.MyViewHolder>(DiffArticle()) {

    class DiffArticle : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            //只有点赞和时间可能存在改变
            return oldItem.collect == newItem.collect && oldItem.niceDate == newItem.niceDate
        }

    }

    class MyViewHolder(private val binding: ItemArticleAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun getBinding(): ItemArticleAdapterBinding {
            return binding
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemArticleAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var article = getItem(position)
        var collect = article.collect
        holder.getBinding().apply {
            tvTime.text = article.niceDate
            tvTitle.text = article.title
            tvSuperChapterName.text = article.superChapterName
        }
        if (collect) {
            holder.getBinding().ivCollect.setBackgroundResource(R.drawable.ic_collect_checked)
        } else {
            holder.getBinding().ivCollect.setBackgroundResource(R.drawable.ic_collect_unchecked_stroke)
        }
        holder.itemView.setOnClickListener {
            mOnItemClickListener?.invoke(position, it)
        }

        holder.getBinding().ivCollect.setOnClickListener {
            mOnItemChildClickListener?.invoke(position, it)
        }

        val type = article.type
        if (type == 1) {
            holder.getBinding().zhiding.visibility = View.VISIBLE
        } else {
            holder.getBinding().zhiding.visibility = View.GONE
        }
        holder.getBinding().author.text = "${article.author}"
    }

    /**
     * item点击事件
     * @param Int 角标
     * @param View 点击的View
     */
    private var mOnItemClickListener: ((Int, View) -> Unit)? = null


    /**
     * item中字view的点击事件，需要子类做具体触发
     * @param Int 角标
     * @param View 点击的View
     */
    private var mOnItemChildClickListener: ((Int, View) -> Unit)? = null


    /**
     * 注册item点击事件
     */
    fun setOnItemClickListener(onItemClickListener: ((Int, View) -> Unit)? = null) {
        this.mOnItemClickListener = onItemClickListener
    }

    fun setonItemChildClickListener(onItemChildClickListener: ((Int, View) -> Unit)? = null) {
        this.mOnItemChildClickListener = onItemChildClickListener
    }
}