package com.yanhao.bean

data class Chapters(
    val apkLink: String,//": "",
    val audit: Int,//": 1,
    val author: String,//": "鸿洋",
    val canEdit: Boolean,//": false,
    val chapterId: Int,//": 408,
    val chapterName: String,//": "鸿洋",
    val collect: Boolean,//": false,
    val courseId: Int,//": 13,
    val desc: String,//": "",
    val descMd: String,//": "",
    val envelopePic: String,//": "",
    val fresh: Boolean,//": false,
    val host: String,//": "",
    val id: Long,//": 18411,
    val link: String,//": "https://mp.weixin.qq.com/s/mPrVnQXQSdXPsaRxhY0ETg",
    val niceDate: String,//": "2021-05-22 00:00",
    val niceShareDate: String,//": "2021-05-24 23:00",
    val shareDate: Long,//": 1621868433000,
    val superChapterName: String,//": "公众号",
    val title: String,//": "聊点面试真相 ，如何提高简历通过率？",
    val type: Int,//": 0,
    val userId: Int,//": -1,
    val visible: Int,//": 1,
    val zan: Int//": 0
)