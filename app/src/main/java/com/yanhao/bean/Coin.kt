package com.yanhao.bean

data class Coin(
    val coinCount: Int = 0,// 451, //总积分
    val rank: Int = 0,// 7, //当前排名
    val userId: Int = 0,// 2,
    val username: String = "" //"x**oyang"
)