package com.yanhao.bean

data class WanResult<T>(val errorMsg: String, val errorCode: Int, val data: T)