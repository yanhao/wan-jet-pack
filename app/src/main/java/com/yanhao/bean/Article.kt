package com.yanhao.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yanhao.db.DBARTICLENAME


//@Entity(tableName = DBARTICLENAME)
data class Article(
//    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val apkLink: String,
    val audit: Int,
    val author: String,
    val canEdit: Boolean,
    val chapterId: Int,
    val chapterName: String,
    val collect: Boolean,
    val courseId: Int,
    val desc: String,
    val descMd: String,
    val link: String,
    val niceDate: String,
    val niceShareDate: String,
    val shareDate: Long,
    val shareUser: String,
    val superChapterId: Int,
    val superChapterName: String,
    val title: String,//": "Gradle学习系列（六）：Gradle 源码解析",
    val type: Int,
    val userId: Long,
    val zan: Int
)