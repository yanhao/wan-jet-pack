package com.yanhao.bean

import android.os.Parcel
import android.os.Parcelable

class UserInfo(
    /**
     * admin : false
     * chapterTops : []
     * collectIds : [10479,12202,12148,10916,12175]
     * email :
     * icon :
     * id : 36628
     * nickname : 18616720137
     * password :
     * publicName : 18616720137
     * token :
     * type : 0
     * username : 18616720137
     */
    val admin: Boolean = false,
    val chapterTops: List<Any> = mutableListOf(),
    val coinCount: Int = 0,
    val collectIds: List<Any> = mutableListOf(),
    val email: String = "",
    val icon: String = "",
    val id: Int = 0,
    val nickname: String = "",
    val password: String = "",
    val publicName: String = "",
    val token: String = "",
    val type: Int = 0,
    val username: String = "",
)