package com.yanhao.bean

sealed class SealedWanResult {
    object Loadding : SealedWanResult()
    data class Success(val value: Any) : SealedWanResult()
    data class Failure(val throwable: Throwable?) : SealedWanResult()
    object Completion : SealedWanResult()
}