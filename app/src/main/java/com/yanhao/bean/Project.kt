package com.yanhao.bean

data class Project(
    val courseId: Int,// 13,
    val id: Int,// 294,
    val name: String,// "完整项目",
    val order: Long,// 145000,
    val parentChapterId: Int,// 293,
    val userControlSetTop: Boolean,// false,
    val visible: Int// 0
)