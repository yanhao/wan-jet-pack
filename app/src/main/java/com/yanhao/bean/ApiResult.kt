package com.yanhao.bean

import com.yanhao.bean.ApiResult.Faiure


//data class WanResult<T>(val errorMsg: String, val errorCode: Int, val data: T)

sealed class ApiResult<out T> {
    object Loadding : ApiResult<Nothing>()
    data class Success<T>(val data: T) : ApiResult<T>()
    data class Faiure(val errorCode: Int, val errorMsg: String) : ApiResult<Nothing>()
    object Completion : ApiResult<Nothing>()
}

data class ResultViewState<T>(
    val state: ApiResult<T> = ApiResult.Loadding
)