package com.yanhao.bean

data class WxArticle(
    val courseId: Int,//": 13,
    val id: Int,//": 408,
    val name: String,//": "鸿洋",
    val order: Long,//": 190000,
    val parentChapterId: Long,//": 407,
    val userControlSetTop: Boolean,//": false,
    val visible: Int//": 1
)