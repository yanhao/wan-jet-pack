package com.yanhao.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.wang.avi.AVLoadingIndicatorView
import com.yanhao.R

class LoadingDialog(context: Context) :
    Dialog(context, R.style.Dialog) {

    private var avi: AVLoadingIndicatorView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_loadding)
        avi = findViewById(R.id.avi)
        startAnim()
    }
    override fun onDetachedFromWindow() {
        super.onAttachedToWindow()
        stopAnim()
    }

    fun startAnim() {
        avi!!.show()
        // or avi.smoothToShow();
    }

    fun stopAnim() {
        avi!!.hide()
        // or avi.smoothToHide();
    }
}