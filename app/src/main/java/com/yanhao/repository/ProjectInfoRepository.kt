package com.yanhao.repository

import com.yanhao.bean.ProjectInfos
import com.yanhao.bean.WanResult
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager

class ProjectInfoRepository {
    suspend fun getProjectTreeInfo(index: Int, cid: Int): WanResult<ProjectInfos> {
        return RetrofitManager.getApiService(ApiService::class.java)
            .getProjectTreeInfo(index = index, cid = cid)
    }
}