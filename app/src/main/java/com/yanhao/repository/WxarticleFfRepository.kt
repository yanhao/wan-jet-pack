package com.yanhao.repository

import com.yanhao.bean.Chapters
import com.yanhao.bean.Chapterss
import com.yanhao.bean.WanResult
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager

class WxarticleFfRepository {
    suspend fun getWxarticleDatas(id: Int, indexPager: Int): WanResult<Chapterss> {
        return RetrofitManager.getApiService(ApiService::class.java)
            .getWxArticleChapters(id, indexPager)
    }
}