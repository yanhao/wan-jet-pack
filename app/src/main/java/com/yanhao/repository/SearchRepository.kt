package com.yanhao.repository

import com.yanhao.bean.WanResult
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.Dispatcher

class SearchRepository {
//    suspend fun search() = flow {
//        val result = WanResult<String>(errorMsg = "ok", errorCode = 0, data = "我是测试数据")
//        emit(result)
//    }.flowOn(Dispatchers.IO)
//        .conflate()

    suspend fun search(pageNum: Int, k: String) =
        flow {
            val result = RetrofitManager.getApiService(ApiService::class.java).search(pageNum, k)
            emit(result)
        }.flowOn(Dispatchers.IO).conflate()
}