package com.yanhao.repository

import com.yanhao.bean.WanResult
import com.yanhao.bean.WxArticle
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager

class NavigationFgRepository {
    suspend fun getWxArticles(): WanResult<MutableList<WxArticle>> {
        return RetrofitManager.getApiService(ApiService::class.java).getWxArticles()
    }
}