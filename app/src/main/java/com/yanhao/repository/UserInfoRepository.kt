package com.yanhao.repository

import com.yanhao.bean.Coin
import com.yanhao.bean.UserInfo
import com.yanhao.bean.WanResult
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class UserInfoRepository {
    fun logint(username: String, password: String): Flow<WanResult<UserInfo>> = flow {
        emit(
            RetrofitManager.getApiService(ApiService::class.java)
                .login(username, password)
        )
    }.flowOn(Dispatchers.IO)


//    suspend fun logint(username: String, password: String): WanResult<UserInfo> =
//        RetrofitManager.getApiService(ApiService::class.java)
//            .login(username, password)

//    suspend fun logint(username: String, password: String): ApiResult<UserInfo> =
//        RetrofitManager.getApiService(ApiService::class.java)
//            .login(username, password)


    suspend fun logout(): WanResult<Any> =
        RetrofitManager.getApiService(ApiService::class.java).logout()

    suspend fun getCoin(): WanResult<Coin> =
        RetrofitManager.getApiService(ApiService::class.java).getCoin()
}