package com.yanhao.repository

import com.yanhao.bean.Project
import com.yanhao.bean.WanResult
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager

class ProjectRepository {

    suspend fun getProjectTabTitle(): WanResult<MutableList<Project>> =
        RetrofitManager.getApiService(ApiService::class.java).getProjectTree()
}