package com.yanhao.repository

import com.yanhao.bean.*
import com.yanhao.http.ApiService
import com.yanhao.http.RetrofitManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class HomeFgRepository {

    fun getBanner(): Flow<WanResult<MutableList<BannerData>>> = flow {
        emit(RetrofitManager.getApiService(ApiService::class.java).getBanner())
    }.flowOn(Dispatchers.IO)

//    suspend fun getBanner(): WanResult<MutableList<BannerData>> {
//        return RetrofitManager.getApiService(ApiService::class.java).getBanner()
//    }
//    suspend fun getBanner(): SealedWanResult<MutableList<BannerData>> {
//        return RetrofitManager.getApiService(ApiService::class.java).getBanner()
//    }


    suspend fun getHomeArticels(pageIndex: Int): WanResult<Articles> {
        return RetrofitManager.getApiService(ApiService::class.java).getHomeArticles(pageIndex)
    }

//    suspend fun getHomeArticelsflow(pageIndex: Int) = flow {
//        val result =
//            RetrofitManager.getApiService(ApiService::class.java).getHomeArticles(pageIndex)
//        emit(result)
//    }.flowOn(Dispatchers.IO).conflate()

    suspend fun getTopArticels(): WanResult<MutableList<Article>> {
        return RetrofitManager.getApiService(ApiService::class.java).getTopHomeArticles()
    }

}